package com.engradiup.oauth2.dao;

import com.engradiup.oauth2.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<User, String> {


    @Override
    List<User> findAll();

    User findOneByUserName(String username);
}


