package com.engradiup.services;

import com.engradiup.dao.UserRepository;
import com.engradiup.model.FactChangeMessage;
import com.engradiup.model.User;
import com.engradiup.websupport.DataExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import static com.engradiup.model.StatusEnum.PENDING;

/**
 * Created by minhthu on 01/08/17.
 */
@Service
public class UserManagementService {

    @Autowired
    UserRepository ur;

    public String signup(String email) {
        sendMessage(FactChangeMessage.builder().key("submit_ca").valueInt(30).build());
        sendMessage(FactChangeMessage.builder().key("longest_treak").valueInt(5).build());
        return email + " template";
    }

    public String addUser(User user) {

        if (ur.findByUserName(user.getUserName()).size() != 0) {
            throw new DataExistsException("user has already exist");
        }
        user.setStatus(PENDING);
        ur.insert(user);
        return user.getUserId();
    }

    public String activateUser(String userId) {
        return null;

    }

    @Autowired
    private KafkaTemplate<String, FactChangeMessage> kafkaTemplate;

    public void sendMessage(FactChangeMessage msg) {
        kafkaTemplate.send("fact1", msg);
    }



}
