package com.engradiup.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by minhthu on 01/08/17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public final class Lecturer extends User {
    private String staffId;
}
