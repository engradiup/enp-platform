package com.engradiup.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by minhthu on 01/08/17.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public final class Student extends User {
    public Student(String name) {
        super();
        courseName = name;
    }

    private String courseName;
    private String activeYear;
}
