package com.engradiup.dao;

import com.engradiup.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;


public interface UserRepository extends MongoRepository<User, String> {

    public List<User> findAll();

    public List<User> findByUserName(String username);


}
