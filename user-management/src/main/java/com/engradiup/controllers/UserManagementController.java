package com.engradiup.controllers;

import com.engradiup.model.Student;
import com.engradiup.model.User;
import com.engradiup.services.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by minhthu on 01/08/17.
 */
@RestController
public class UserManagementController {

    @Autowired
    UserManagementService ums;

    @RequestMapping(value = "/template/{email}", method = RequestMethod.GET)
    public String template(@PathVariable("email") String email) {

//        User u = User.builder().userId("100").
//                associateUserId("3").
//                dateOfBirth(LocalDate.now()).
//                firstName("A").
//                lastName("B").
//                password("aaa").
//                role(RoleEnum.STUDENT).
//                status(StatusEnum.PENDING).
//                userName("user1").build();
// stuff to test mongo, but now all ok
//        ums.addUser(u);

//        throw new IllegalArgumentException("stuff");



//        BasicAWSCredentials b = new BasicAWSCredentials("AKIAID3HDUSOVL3KOLBQ", "bJKo1IN5lS2ORwNFqEn6ZSHEFMGE14FRXF+Z14tK");

//        AmazonSNSClient snsClient = new AmazonSNSClient(b).withRegion(Regions.US_WEST_2);
//        String msg = "Welcome to engradiup, you have sucessfully signedup";
//        PublishRequest publishRequest = new PublishRequest("arn:aws:sns:us-west-2:957349410460:EngradiupPush", msg);
//        PublishResult publishResult = snsClient.publish(publishRequest);
////print MessageId of message published to SNS topic
//        System.out.println("MessageId - " + publishResult.getMessageId());





        return ums.signup(email);

//        AWSAccessKeyId=AKIAID3HDUSOVL3KOLBQ
//        AWSSecretKey=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



    }

    @RequestMapping(value = "/template1", method = RequestMethod.POST)
    public ResponseEntity<Student> template1(@RequestBody String email) {
        Student s = new Student(email);
        return new ResponseEntity<Student>(s, HttpStatus.OK);
    }


    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public ResponseEntity<String> signup(@RequestBody User user) {
        return new ResponseEntity<>(ums.addUser(user), HttpStatus.OK);
    }


}

