package com.engradiup;

import com.engradiup.dao.UserRepository;
import com.engradiup.model.RoleEnum;
import com.engradiup.model.StatusEnum;
import com.engradiup.model.User;
import com.engradiup.services.UserManagementService;
import com.engradiup.websupport.DataExistsException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

@ActiveProfiles({"test", "unit"})
@RunWith(SpringRunner.class)
@SpringBootTest

public class UserManagementServiceTest {
    @Autowired
    private UserManagementService ums;
    @Autowired
    private UserRepository ur;


    @Before
    public void setUp() {
        ur.deleteAll();

        ur.save(User.builder().
                userId("1").
                associateUserId("1").
                dateOfBirth(LocalDate.now()).
                firstName("A").
                lastName("B").
                password("aaa").
                role(RoleEnum.STUDENT).
                status(StatusEnum.PENDING).
                userName("user1").build());

        ur.save(User.builder().
                userId("2").
                associateUserId("2").
                dateOfBirth(LocalDate.now()).
                firstName("A").
                lastName("B").
                password("aaa").
                role(RoleEnum.STUDENT).
                status(StatusEnum.PENDING).
                userName("user2").build());

    }

    @After
    public void tearDown() {
        ur.deleteAll();


    }

    @Test
    public void addUserTest() {
//        assertEquals(ur.findAll().size(), 2);
        User u = User.builder().userId("3").
                associateUserId("3").
                dateOfBirth(LocalDate.now()).
                firstName("A").
                lastName("B").
                password("aaa").
                role(RoleEnum.STUDENT).
                status(StatusEnum.PENDING).
                userName("user3").build();
        ums.addUser(u);
        assertEquals(ur.findAll().size(), 3);
    }

    @Test
    public void addUserDuplicateTest() {
        User u = User.builder().userId("3").
                associateUserId("3").
                dateOfBirth(LocalDate.now()).
                firstName("A").
                lastName("B").
                password("aaa").
                role(RoleEnum.STUDENT).
                status(StatusEnum.PENDING).
                userName("user1").build();
        try {
            ums.addUser(u);
        } catch (DataExistsException e) {
            assertEquals(e.getMessage(), "user has already exist");
        } finally {
            assertEquals(ur.findAll().size(), 2);

        }


    }

    @Test
    public void testKafka(){


    }

}
