package com.engradiup.websupport;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class DataExistsException extends RuntimeException {
    public DataExistsException() {
        super();
    }

    public DataExistsException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataExistsException(String message) {
        super(message);
    }

    public DataExistsException(Throwable cause) {
        super(cause);
    }

}
