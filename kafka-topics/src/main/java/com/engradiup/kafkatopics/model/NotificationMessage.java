package com.engradiup.kafkatopics.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public final class NotificationMessage {

    private String userId;
    private String header;
    private String body;
    private String link;
    private String sendTo;
}
