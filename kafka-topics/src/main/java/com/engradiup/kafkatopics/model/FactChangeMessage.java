package com.engradiup.kafkatopics.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FactChangeMessage {
    private String userId;
    private String key;
    private String value;
    private int valueInt;
    private String device;
}
