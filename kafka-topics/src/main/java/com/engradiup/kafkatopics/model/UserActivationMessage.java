package com.engradiup.kafkatopics.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public final class UserActivationMessage {
    private int age;
    private String userId;
    private Date dateActivate;
}
