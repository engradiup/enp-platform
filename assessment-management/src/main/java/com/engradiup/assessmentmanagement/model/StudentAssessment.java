package com.engradiup.assessmentmanagement.model;

import io.swagger.model.ScheduledTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StudentAssessment {
    private Assessment assessment;
    private DateTimePair scheduledTime;

}
