package com.engradiup.assessmentmanagement.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public final class Module {

    @Id
    private String id;
    private String moduleName;
    private String moduleDescription;
    private List<String> assessmentList;
    private List<String> tags;
}
