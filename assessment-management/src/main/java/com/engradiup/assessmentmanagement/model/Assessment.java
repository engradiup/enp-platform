package com.engradiup.assessmentmanagement.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public final class Assessment {
    private String id;
    private String assessmentName;
    private double weight;
    private List<String> topicsCovered;
}
