package com.engradiup.assessmentmanagement.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.LocalDate;
import org.springframework.data.annotation.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public final class AssessmentEvent {

    @Id
    private String id;
    private String assessmentId = null;
    private String studentId = null;
    private String moduleId = null;
    private Double result = null;
    private LocalDate takenDate = null;
    private LocalDate scheduledDate = null;
    private Double weight = null;

}
