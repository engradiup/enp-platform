package com.engradiup.assessmentmanagement.model;

/**
 * Created by minhthu on 01/08/17.
 */
public enum RoleEnum {
    ADMIN,
    STUDENT,
    LECTURER
}
