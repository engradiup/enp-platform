package com.engradiup.assessmentmanagement.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.time.LocalDate;

/**
 * Created by minhthu on 01/08/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User{
    @Id
    private String userId;
    private String userName;
    private String password;
    private String firstName;
    private String lastName;
    private String dateOfBirth;
    private RoleEnum role;
    private StatusEnum status;
    private String associateUserId;
}
