package com.engradiup.assessmentmanagement.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StudentModule {

    @Id
    private String id;
    private String studentId;
    private String moduleId;
    private String lecturerId;
    private double average;
    private List<AssessmentEntry > assessments;

    @Data
    public static class AssessmentEntry{

        public String assessmentId;
        public DateTimePair time;

    }

}
