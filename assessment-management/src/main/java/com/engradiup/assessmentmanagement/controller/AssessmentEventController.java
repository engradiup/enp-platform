package com.engradiup.assessmentmanagement.controller;


import com.engradiup.assessmentmanagement.model.AssessmentEvent;
import com.engradiup.assessmentmanagement.service.AssessmentEventService;
import io.swagger.model.AssessmentEventDTO;
import io.swagger.model.StudentModuleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AssessmentEventController {


    @Autowired
    AssessmentEventService assessmentEventService;

    @RequestMapping(value = "/result/{userId}", method = RequestMethod.POST)
    ResponseEntity<String> assessmentEventPost(@PathVariable(value="userId") String userId, @RequestBody AssessmentEventDTO assessmentEventDTO){

        String id= assessmentEventService.insertRecord(fromDTO(assessmentEventDTO),assessmentEventDTO.getDevice());
        return ResponseEntity.ok(id);
    }

    AssessmentEvent fromDTO(AssessmentEventDTO assessmentEventDTO){
        return AssessmentEvent.builder().assessmentId(assessmentEventDTO.getAssessmentId())
                .moduleId(assessmentEventDTO.getModuleId())
                .studentId(assessmentEventDTO.getStudentId())
                .result(assessmentEventDTO.getResult())
                .weight(assessmentEventDTO.getWeight()).build();

    }



}
