package com.engradiup.assessmentmanagement.controller;


import com.engradiup.assessmentmanagement.model.StudentModule;
import com.engradiup.assessmentmanagement.service.ModuleService;
import io.swagger.api.ModulesApi;
import io.swagger.model.ModuleDTO;
import io.swagger.model.StudentAssessmentDTO;
import io.swagger.model.StudentModuleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ModuleController {


    @Autowired
    ModuleService moduleService;

    StudentModuleDTO toStudentModuleDTO(StudentModule studentModule){

        StudentModuleDTO moduleDTO= new StudentModuleDTO();
        return moduleDTO.studentId(studentModule.getStudentId()).moduleId(studentModule.getModuleId()).average(0).lecturerId(studentModule.getLecturerId())
                .assessments(null);
    }

    StudentModule fromStudentModuleDTO(StudentModuleDTO studentModuleDTO)
    {
        return StudentModule.builder().build();
    }


    @RequestMapping(value = "/modules/{username}", method = RequestMethod.GET)
    public ResponseEntity<List<StudentModuleDTO>> modulesUsernameGet(@PathVariable("username") String username) {


        System.out.println(username);
            List<StudentModuleDTO> modules=moduleService.getStudentModules(username).stream().map(a -> toStudentModuleDTO(a)).collect(Collectors.toList());
        System.out.println(modules);
        return ResponseEntity.ok().body(modules);
    }

    @RequestMapping(value="/tags/{moduleId}", method = RequestMethod.POST)
    public ResponseEntity<List<String>> modulesTagGet(@PathVariable("moduleId") String moduleId){

        if (moduleId== null) throw new IllegalArgumentException("bad module ID format");

        return ResponseEntity.ok().body(moduleService.getTopicsCoveredInModule(moduleId));
    }

//    @RequestMapping(value = "average/{moduleId}/{userId}")



}
