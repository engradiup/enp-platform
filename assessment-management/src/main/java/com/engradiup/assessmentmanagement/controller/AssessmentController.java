package com.engradiup.assessmentmanagement.controller;


import com.engradiup.assessmentmanagement.model.StudentAssessment;
import com.engradiup.assessmentmanagement.service.AssessmentService;
import io.swagger.api.ModulesApi;
import io.swagger.model.StudentAssessmentDTO;
import io.swagger.model.StudentModuleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AssessmentController{

    @Autowired
    AssessmentService assessmentService;

    @RequestMapping(value = "/schedules/{moduleId}/{userId}", method = RequestMethod.GET)
    public ResponseEntity<List<StudentAssessmentDTO>> modulesUsernameGet(@PathVariable("moduleId") String moduleId,@PathVariable("username") String username) {

        List<StudentAssessment> temp=assessmentService.getAssessmentsForModule(moduleId,username);



        return ResponseEntity.ok().body(null);
    }

    StudentAssessmentDTO toStudentAssessmentDTO(StudentAssessment studentAssessment){

        return (new StudentAssessmentDTO());
    }



//    @RequestMapping()





}
