package com.engradiup.assessmentmanagement.service;

import com.engradiup.assessmentmanagement.dao.ModulesRepository;
import com.engradiup.assessmentmanagement.dao.StudentModulesRepository;
import com.engradiup.assessmentmanagement.dao.UserRepository;
import com.engradiup.assessmentmanagement.model.Assessment;
import com.engradiup.assessmentmanagement.model.Module;
import com.engradiup.assessmentmanagement.model.StudentModule;
import com.engradiup.assessmentmanagement.model.StudentModule.AssessmentEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModuleService {

    @Autowired
    StudentModulesRepository studentModulesRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    ModulesRepository modulesRepository;



    public List<StudentModule> getStudentModules(String username){


        if (userRepository.findByUserName(username).size()==0){
            throw new IllegalArgumentException("user not exists"+username);

        }

        String userId= userRepository.findByUserName(username).get(0).getUserId();
        System.out.printf("userId: "+ userId);

        if (studentModulesRepository.findByStudentId(userId).size()==0){
            throw new IllegalArgumentException("empty modules not exists");


        }

        List<StudentModule> thingy= studentModulesRepository.findByStudentId(userId);
        System.out.printf("thingy : "+ thingy);
        return thingy;
    }

    public List<String> getTopicsCoveredInModule(String moduleId){


        if (modulesRepository.findById(moduleId)== null){

            throw new IllegalArgumentException("no modules found");
        }

//        List<AssessmentEntry> entries =studentModulesRepository.findByModuleId(moduleId).get(0).getAssessments();

        return modulesRepository.findById(moduleId).get(0).getTags();
    }


}
