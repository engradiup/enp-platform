package com.engradiup.assessmentmanagement.service;


import com.engradiup.assessmentmanagement.dao.AssessmentEventRepository;
import com.engradiup.assessmentmanagement.model.AssessmentEvent;
import com.engradiup.kafkatopics.model.FactChangeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AssessmentEventService {

    @Autowired
    AssessmentEventRepository aer;
    public String insertRecord(AssessmentEvent assessmentEvent,String device){



        String id=aer.save(assessmentEvent).getId();

        FactChangeMessage a = FactChangeMessage.builder().device(device).key("submit_ca").value(assessmentEvent.getModuleId()).valueInt((int)Math.round(assessmentEvent.getResult())).userId(assessmentEvent.getStudentId()).build();

        sendMessage(a);

        return id;
    }


    @Autowired
    private KafkaTemplate<String, FactChangeMessage> kafkaTemplate;

    public void sendMessage(FactChangeMessage msg) {
        kafkaTemplate.send("fact1", msg);
    }


}
