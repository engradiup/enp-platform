package com.engradiup.assessmentmanagement.service;

import com.engradiup.assessmentmanagement.dao.AssessmentsRepository;
import com.engradiup.assessmentmanagement.dao.ModulesRepository;
import com.engradiup.assessmentmanagement.dao.StudentModulesRepository;
import com.engradiup.assessmentmanagement.model.Assessment;
import com.engradiup.assessmentmanagement.model.DateTimePair;
import com.engradiup.assessmentmanagement.model.StudentAssessment;
import com.engradiup.assessmentmanagement.model.StudentModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AssessmentService {
    @Autowired
    AssessmentsRepository aR;

    @Autowired
    StudentModulesRepository sMR;



    public List<StudentAssessment> getAssessmentsForModule(String moduleId,String userId){


        if (sMR.findByModuleIdAndStudentId(moduleId,userId)== null){
            throw new IllegalArgumentException("invalid module");
        }

        List<StudentModule.AssessmentEntry> temp= sMR.findByModuleIdAndStudentId(moduleId,userId).get(0).getAssessments();

        List<StudentAssessment> thingy= new ArrayList<>();
        for(StudentModule.AssessmentEntry t:temp){
            if (aR.findById(t.getAssessmentId())== null){

                throw new IllegalArgumentException("weird thing happened");
            }

            thingy.add(StudentAssessment.builder().assessment(aR.findById(t.getAssessmentId()).get(0)).
                    scheduledTime(DateTimePair.builder().time(t.getTime().getTime()).date(t.getTime().getDate()).build()).build());

        }

        return thingy;
    }



}
