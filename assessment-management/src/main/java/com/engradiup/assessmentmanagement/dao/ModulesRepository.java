package com.engradiup.assessmentmanagement.dao;

import com.engradiup.assessmentmanagement.model.Module;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ModulesRepository extends MongoRepository<Module,String>{

    List<Module> findById(String moduleId);
}
