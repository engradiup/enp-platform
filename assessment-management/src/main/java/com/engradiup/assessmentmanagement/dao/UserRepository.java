package com.engradiup.assessmentmanagement.dao;

import com.engradiup.assessmentmanagement.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    public List<User> findAll();

    public List<User> findByUserName(String username);


}
