package com.engradiup.assessmentmanagement.dao;

import com.engradiup.assessmentmanagement.model.StudentModule;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentModulesRepository extends MongoRepository<StudentModule, String> {

     List<StudentModule> findAll();
     List<StudentModule> findByStudentId(String userId);
     List<StudentModule> findByModuleIdAndStudentId(String moduleId, String studentId);
}
