package com.engradiup.assessmentmanagement.dao;


import com.engradiup.assessmentmanagement.model.AssessmentEvent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AssessmentEventRepository extends MongoRepository<AssessmentEvent, String>{

    @Override
    AssessmentEvent save (AssessmentEvent assessmentEvent);

    AssessmentEvent findOneById(String id);
}
