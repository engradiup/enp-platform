package com.engradiup.assessmentmanagement.dao;

import com.engradiup.assessmentmanagement.model.Assessment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AssessmentsRepository extends MongoRepository<Assessment, String> {

    List<Assessment> findById(String assessmentId);
}
