package com.engradiup.assessmentmanagement;

import com.engradiup.assessmentmanagement.dao.StudentModulesRepository;
import com.engradiup.assessmentmanagement.service.ModuleService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.Collectors;

@ActiveProfiles({"test", "unit"})
@RunWith(SpringRunner.class)
@SpringBootTest
public class ModuleManagementServiceTest {

    @Autowired
    ModuleService moduleService;

    @Autowired
    StudentModulesRepository smr;

    @Test
    @Ignore
    public void test1(){

        moduleService.getStudentModules("test");

    }
}
