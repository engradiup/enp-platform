package com.engradiup.rulesengine;

import com.engradiup.rulesengine.models.Fact;
import com.engradiup.rulesengine.services.RulesEngineServices;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collections;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RulesEngineApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Autowired
	RulesEngineServices rulesEngineServices;

	@Test
	public void test1(){

		Fact f= Fact.builder().key("a").value("b").build();

		rulesEngineServices.getStuff(Collections.singletonList(f));

	}

	@Test

	public void test(){
		ArrayList<Fact> facts = new ArrayList<>();

		facts.add(Fact.builder().key("submit_ca").valueInt(80).build());
		facts.add(Fact.builder().key("longest_treak").valueInt(3).build());
		facts.add(Fact.builder().key("hours_on_Netflix").valueInt(6).build());
		facts.add(Fact.builder().key("hour_of_day").valueInt(20).build());

		rulesEngineServices.getStuff(facts);


	}

}
