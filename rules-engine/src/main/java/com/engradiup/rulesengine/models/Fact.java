package com.engradiup.rulesengine.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderMethodName = "builder")
public class Fact {
    private String key;
    private String value;
    private int valueInt;
    private String device;

}
