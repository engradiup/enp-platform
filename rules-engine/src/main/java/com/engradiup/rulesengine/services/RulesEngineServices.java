package com.engradiup.rulesengine.services;

import com.engradiup.kafkatopics.model.NotificationMessage;
import com.engradiup.rulesengine.helpers.RulesHelper;
import com.engradiup.rulesengine.models.Fact;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RulesEngineServices {

    @Autowired
    private KieContainer kieContainer;


    public void getStuff(List<Fact> fact){
        KieSession kieSession = kieContainer.newKieSession();


            for (Fact f: fact)
            kieSession.insert(f);
            kieSession.fireAllRules();
            kieSession.dispose();
            System.out.println(fact.get(0).getValue());
    }

    @Autowired
    private KafkaTemplate<String, NotificationMessage> kafkaTemplate;

    public void sendMessage(NotificationMessage msg) {
        kafkaTemplate.send("fact1", msg);
    }



    public static void sendPush(NotificationMessage noti){


        RulesHelper.sendQueue(noti);

//        sendMessage(noti);
        System.out.println(noti);
        System.out.println("rules fired");

    }
}
