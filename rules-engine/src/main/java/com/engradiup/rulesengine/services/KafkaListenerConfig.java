package com.engradiup.rulesengine.services;

import com.engradiup.kafkatopics.model.FactChangeMessage;
import com.engradiup.rulesengine.models.Fact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;

import java.util.ArrayList;

@Configuration
@EnableKafka
public class KafkaListenerConfig {

    static ArrayList<Fact> facts = new ArrayList<>();
    @Autowired RulesEngineServices rulesEngineServices;
    @KafkaListener(topics = "fact1", group = "foo")
    public void listen(FactChangeMessage message) {

        System.out.println("Received Messasge: " + message);
        Fact f=Fact.builder().device(message.getDevice()).key(message.getKey()).value(message.getValue()).valueInt(message.getValueInt()).build();
        facts.add(f);
        rulesEngineServices.getStuff(facts);
    }
}