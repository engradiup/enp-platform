package com.engradiup.rulesengine.helpers;

import com.engradiup.kafkatopics.model.NotificationMessage;
import net.minidev.json.JSONObject;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

public class RulesHelper {



    public static void sendQueue(NotificationMessage noti){
        System.out.println( noti);

        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
        post.setHeader("Content-type", "application/json");
        post.setHeader("Authorization", "key=AIzaSyA8Au0xGBO93QGweyt6Iz_7uCS855ln9lY");

        JSONObject message = new JSONObject();
        message.put("to", noti.getSendTo());
        message.put("priority", "high");

        JSONObject notification = new JSONObject();
        notification.put("title", noti.getHeader());
        notification.put("body", noti.getBody());

        message.put("notification", notification);

        post.setEntity(new StringEntity(message.toString(), "UTF-8"));
        HttpResponse response = null;
        try {
            response = client.execute(post);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(response);
        System.out.println(message);

    }



}
